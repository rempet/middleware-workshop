<?php

/**
 * This file is part of Boozt Platform
 * and belongs to Boozt Fashion AB.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 */

declare(strict_types=1);

namespace App\DataObject\QueryResult;

class ListResult implements \IteratorAggregate
{
    /** @var ListItem[] */
    private $items;

    public function __construct(ListItem ...$items)
    {
        $this->items = $items;
    }

    public function getItem(string $name): ?ListItem
    {
        foreach ($this->items as $item) {
            if ($item->getName() === $name) {
                return $item;
            }
        }

        return null;
    }

    public function getIterator(): iterable
    {
        return new \ArrayIterator($this->items);
    }
}
