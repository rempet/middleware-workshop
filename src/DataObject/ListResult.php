<?php

/**
 * This file is part of Boozt Platform
 * and belongs to Boozt Fashion AB.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 */

declare(strict_types=1);

namespace App\DataObject;

class ListResult implements \JsonSerializable
{

    public function jsonSerialize(): string
    {
        // TODO: Implement jsonSerialize() method.
    }
}
