<?php

/**
 * This file is part of Boozt Platform
 * and belongs to Boozt Fashion AB.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 */

declare(strict_types=1);

namespace App\Controller;

use App\Service\ListService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/list")
 */
class ListController extends AbstractController
{
    /**
     * @Route(name="list_app.get_list", methods={"GET"})
     *
     * @param ListService $listService
     *
     * @return Response
     */
    public function getList(ListService $listService): Response
    {
        return new JsonResponse(json_encode($listService->getList()));
    }
}
