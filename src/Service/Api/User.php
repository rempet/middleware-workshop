<?php

/**
 * This file is part of Boozt Platform
 * and belongs to Boozt Fashion AB.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 */

declare(strict_types=1);

namespace App\Service\Api;

use GuzzleHttp\Client;

class User
{
    /** @var Client */
    private $client;

    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    public function getFullName(int $id): string
    {
        $response = $this->client->get(
            sprintf('user/%d/fullname', $id)
        );

        return (string) $response->getBody();
    }
}
