<?php

/**
 * This file is part of Boozt Platform
 * and belongs to Boozt Fashion AB.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 */

declare(strict_types=1);

namespace App\Service;

use App\DataObject\QueryResult\ListItem;
use App\DataObject\QueryResult\ListResult;
use App\Repository\ListRepository;
use App\Service\Api\User as UserApi;

class ListService
{
    /** @var ListRepository */
    private $listRepository;

    /** @var UserApi */
    private $userApi;

    public function __construct(
        ListRepository $listRepository,
        UserApi $userApi
    )
    {
        $this->listRepository = $listRepository;
        $this->userApi = $userApi;
    }


    public function getList(): ListResult
    {
        $list = $this->listRepository->getList();

        foreach ($list as $item) {
            /** @var ListItem $item */
            $item->setFullName(
                $this->userApi->getFullName(
                    $item->getUserId()
                )
            );
        }

        return $list;
    }
}
